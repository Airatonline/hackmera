package com.d1sc0ver.hackmera

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.d1sc0ver.hackmera.Temp.EDDYSTONE_SERVICE_UUID
import com.d1sc0ver.hackmera.Temp.beaconTree
import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver
import com.lemmingapex.trilateration.TrilaterationFunction
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity() {

    private lateinit var scan: BluetoothLeScanner
    lateinit var customView: CustomView
    private val scanFilters = ArrayList<ScanFilter>()
    private lateinit var settings: ScanSettings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        val at = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                val a = result.scanRecord!!.serviceData
                val temp = a[EDDYSTONE_SERVICE_UUID]

                //getting data from BLE
                val int = temp?.get(12)?.let { Temp.getByte(it, temp[13]) }
                val x = temp?.get(14)?.let { Temp.getByte(it, temp[15]) }
                val y = temp?.get(16)?.let { Temp.getByte(it, temp[16]) }
                val rssi = result.rssi + 41
                if (int == null || x == null || y == null) {
                    return
                }
                Log.d("AAA", "id: " + int + "|x: " + x + "|y: " + y + "rssi: " + rssi)
                beaconTree.add(EntityBeacon(int, x, y, rssi))
                if (beaconTree.getBeacons().size == 8) {
                    val positions = arrayOf(
                        doubleArrayOf(
                            beaconTree.getBeacons()[0].x.toDouble(),
                            beaconTree.getBeacons()[0].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[1].x.toDouble(),
                            beaconTree.getBeacons()[1].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[2].x.toDouble(),
                            beaconTree.getBeacons()[2].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[3].x.toDouble(),
                            beaconTree.getBeacons()[3].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[4].x.toDouble(),
                            beaconTree.getBeacons()[4].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[5].x.toDouble(),
                            beaconTree.getBeacons()[5].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[6].x.toDouble(),
                            beaconTree.getBeacons()[6].y.toDouble()
                        ),
                        doubleArrayOf(
                            beaconTree.getBeacons()[7].x.toDouble(),
                            beaconTree.getBeacons()[7].y.toDouble()
                        )
                    )

                    val distances = doubleArrayOf(
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[0].rssi.toFloat(),
                            -41F,
                            1.7.toFloat()
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[1].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[2].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[3].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[4].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[5].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[6].rssi.toFloat(),
                            -41F,
                            1.7F
                        ),
                        Temp.calculateDistance(
                            beaconTree.getBeacons()[7].rssi.toFloat(),
                            -41F,
                            1.7F
                        )
                    )

                    val solver = NonLinearLeastSquaresSolver(
                        TrilaterationFunction(positions, distances),
                        LevenbergMarquardtOptimizer()
                    )
                    val optimum = solver.solve()
                    val res = optimum.point.toArray()
                    customView.putUser(res[0].roundToInt(), res[1].roundToInt())

                }
            }
        }

        scan.startScan(scanFilters, settings, at)

    }

    private fun init() {
        val map = findViewById<ImageView>(R.id.map)
        map.setImageResource(R.drawable.map)

        customView = findViewById(R.id.user)
        customView.invalidate()

        getPermissions()

        val bluetoothAdapter: BluetoothAdapter
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
        scan = bluetoothAdapter.bluetoothLeScanner
        scanFilters.add(ScanFilter.Builder().setServiceUuid(EDDYSTONE_SERVICE_UUID).build())
        settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_BALANCED).build()
    }

    private fun getPermissions() {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
        if (ContextCompat.checkSelfPermission(
                this,
                permissions[0]
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@MainActivity,
                permissions,
                1
            )
        }
    }


}


