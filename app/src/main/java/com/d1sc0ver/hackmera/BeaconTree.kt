package com.d1sc0ver.hackmera

class BeaconTree {

    private val beaconData = ArrayList<EntityBeacon>()

    fun add(element: EntityBeacon): Boolean {
        if (beaconData.size == 0) {
            beaconData.add(element)
            return true
        }
        var res = true
        with(beaconData.iterator()) {
            forEach {
                if (it.id == element.id) {
                    remove()
                    res = false
                }
            }
        }
        beaconData.add(element)
        return res

    }

    fun getBeacons(): ArrayList<EntityBeacon> {
        return beaconData
    }
}
