package com.d1sc0ver.hackmera

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color.GREEN
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View


class CustomView(context: Context, attrs: AttributeSet) : View(context, attrs) {
    private var paint: Paint = Paint()
    private var bPaint: Paint = Paint()
    var x = 0
    var y = 0
    private val scale = 2.5F
    private val startX = 100
    private val startY = 1415
    fun putUser(x: Int, y: Int) {
        this.x = x
        this.y = y
        invalidate()
    }

    private lateinit var canvas: Canvas

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        this.canvas = canvas
        paint.color = GREEN

        //User
        canvas.drawCircle(
            (startX + y.toFloat()) * scale,
            (startY - x.toFloat()) * scale,
            60f,
            paint
        )

        //Beacons

        canvas.drawCircle((100 + 0f) * scale, (183f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 0f) * scale, (655f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 0f) * scale, (905f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 0f) * scale, (1415f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 771f) * scale, (130f) * scale, (40f), bPaint)
        canvas.drawCircle((100 + 771f) * scale, (603f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 771f) * scale, (927f) * scale, 40f, bPaint)
        canvas.drawCircle((100 + 771f) * scale, (1402f) * scale, 40f, bPaint)

    }
}