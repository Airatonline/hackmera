package com.d1sc0ver.hackmera;

public class EntityBeacon {
    EntityBeacon(int id, int x, int y, int rssi) {
        this.id = id;
        this.rssi = rssi;
        this.x = x;
        this.y = y;
    }

    int id;
    int x;
    int y;
    int rssi;
}
