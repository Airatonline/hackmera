package com.d1sc0ver.hackmera;

import android.os.ParcelUuid;

public class Temp {
    static int getByte(byte a1, byte a2) {
        return ((a1 & 0xff) << 8) | (a2 & 0xff);
    }

    static final ParcelUuid EDDYSTONE_SERVICE_UUID = ParcelUuid.fromString("0000FEAA-0000-1000-8000-00805F9B34FB");
    static BeaconTree beaconTree = new BeaconTree();

    public static Double calculateDistance(float rssi, float calibratedRssi, float pathLossFactor) {
        return Math.pow(10, (calibratedRssi - rssi) / (10 * pathLossFactor));
    }

}

